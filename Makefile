all: bin/cliente bin/kvstore
IDIR =./include
CFLAGS=-I$(IDIR)

ODIR=obj


LIBS=-lpthread

_DEPS = kv_store.h nodoKvstore.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = kvstore.o crear_kvStore.o free_kvStore.o get_kvStore.o hash_kvStore.o put_kvStore.o remove_kvStore.o nodoKvstore.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

$(ODIR)/%.o: src/%.c $(DEPS)
	gcc -c -o $@ $< $(CFLAGS)


bin/kvstore: $(OBJ) $(DEPS)
	gcc -Wall -I./include $(OBJ) -o $@  $(LIBS)





obj/cliente.o: src/cliente.c
	gcc -c -o $@ $^ 
bin/cliente: obj/cliente.o
	gcc -Wall -o $@ $^

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ bin/* 


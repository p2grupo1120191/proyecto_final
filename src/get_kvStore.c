#include "kv_store.h"
// Implementacion funcion get_kvstore
//Si se hace GET, REMOVE, DELETE a un kv_store que no existe, retornar error.

void *get_kvStore(kvStore *kvs, char *clave){
	if(kvs==NULL || kvs->buckets == NULL || clave == NULL || clave[0]=='\0'){
		return NULL;
	}

	unsigned long indice = hash_kvStore((unsigned char *)clave)%(unsigned long)kvs->numeroBuckets;
	
	void *result = NULL;
	pthread_mutex_lock(&kvs->mutex);
	if(kvs->buckets[indice]->valor!=NULL){
		result = kvs->buckets[indice]->valor;
	}

	pthread_mutex_unlock(&kvs->mutex);


	return result;
}

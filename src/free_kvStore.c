#include "kv_store.h"

// Implementacion funcion free_kvstore 
void free_kvStore(kvStore *kvs){
	if(kvs==NULL || kvs->buckets == NULL){
		return;
	}
	pthread_mutex_lock(&kvs->destroy_mutex);

	for(int i = 0; i<kvs->numeroBuckets;i++){
		if(kvs->buckets[i]!=NULL && kvs->buckets[i]->clave!=NULL){
			free(kvs->buckets[i]->clave);
			kvs->buckets[i]->valor = NULL;
		}
		free(kvs->buckets[i]);
	}
	free(kvs->id);
	free(kvs->buckets);
	pthread_mutex_unlock(&kvs->destroy_mutex);
	pthread_mutex_destroy(&kvs->destroy_mutex);
	free(kvs);
}

#include "kv_store.h"
//funcion put_kvStore
//Si se hace un PUT a un kv_store que no existe, se debe crear automáticamente
void put_kvStore(kvStore *kvs, char *clave, void *valor){
	if(kvs==NULL || kvs->buckets == NULL || clave == NULL || clave[0]=='\0' || valor == NULL){
		return;
	}
	unsigned long indice = hash_kvStore((unsigned char *)clave)%(unsigned long)kvs->numeroBuckets;
	
	//int kth_lock = indice / 100;
	
	if(kvs->buckets[indice]->clave!=NULL){
		pthread_mutex_lock(&kvs->mutex);
		free(kvs->buckets[indice]->clave);
		pthread_mutex_unlock(&kvs->mutex);


	}
	pthread_mutex_lock(&kvs->mutex);
	kvs->buckets[indice]->clave = strdup(clave);
	if(kvs->buckets[indice]->clave==NULL){
		free(clave);
		pthread_mutex_unlock(&kvs->mutex);
		return;
	}

	kvs->elementos++;
	kvs->buckets[indice]->valor = valor;
	pthread_mutex_unlock(&kvs->mutex);

}

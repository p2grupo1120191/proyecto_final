#include <nodoKvstore.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
//funciones Manejo de solicitudes Kv_Store
void crearlistakvStore(listakvStore *l, int tamanioElemento, liberarEspaciokvStore liberar) {

	assert(tamanioElemento > 0);

	l -> tamanioLista = 0;
	l -> tamanioElemento = tamanioElemento;
	l -> inicio = l -> fin = NULL;
	l -> liberar = liberar;

}

void destruirlistakvStore(listakvStore *l) {

	if (l != NULL) {
		while (l -> inicio != NULL) {
			eliminarPrimerkvStore(l);
		}

	}

}

nodoKv * crearnodoKv(listakvStore *l, kvStore *c) {

	if (l != NULL) {

		nodoKv *nuevoNodo = (nodoKv *) malloc(sizeof(nodoKv));
		nuevoNodo -> tabla = c;
		//memcpy(nuevoNodo -> tabla, c, l -> tamanioElemento);
		nuevoNodo->siguiente = NULL;
		return nuevoNodo;

	}

	return NULL;

}

nodoKv *obtenerPrimerkvStore(listakvStore *l) {

	if (l != NULL) {

		return l -> inicio;

	}

	return NULL;

}

nodoKv *obtenerUltimokvStore(listakvStore *l) {

	if (l != NULL) {

		return l -> fin;

	}

	return NULL;

}

int obtenerCantidadkvStores(listakvStore *l) {

	if (l != NULL) {
		return l -> tamanioLista;
	}
	return -1;

}

bool estaVaciakvStores(listakvStore *l) {

	if (l != NULL) {
		return (obtenerCantidadkvStores(l) == 0);
	}
	return false;

}

void insertarkvStoreAlInicio(listakvStore *l, kvStore *c) {

	if (l != NULL) {

		nodoKv *nuevoNodo = crearnodoKv(l, c);
		if (estaVaciakvStores(l)) {
			l -> inicio = l -> fin = nuevoNodo;
		}else {
			l -> inicio -> previo = nuevoNodo;
			nuevoNodo -> siguiente = l -> inicio;
			l -> inicio = nuevoNodo;
		}
		l -> tamanioLista++;

	}

}

void insertarkvStoreAlFinal(listakvStore *l, kvStore *c) {

	if (l != NULL) {	

		nodoKv *nuevoNodo = crearnodoKv(l, c);

		if (estaVaciakvStores(l)) {
			l -> inicio = l -> fin = nuevoNodo;
		}

		else {
			l -> fin -> siguiente = nuevoNodo;
			nuevoNodo -> previo = l -> fin;
			l -> fin = nuevoNodo;
		}
		l -> tamanioLista++;

	}

}

void eliminarPrimerkvStore(listakvStore *l) {

	if (l != NULL) {

		if (!estaVaciakvStores(l)) {

			nodoKv *actual;

			actual = l -> inicio;

			if (actual == l -> fin) {

				l -> inicio = l -> fin = NULL;

			}
			else {

				l -> inicio = actual -> siguiente;

				actual -> siguiente = NULL;

				l -> inicio -> previo = NULL;

			}


			liberarMemoriakvStore(l, actual);

		}

	}

}

void eliminarUltimokvStore(listakvStore *l) {

	if (l != NULL) {

		if (!estaVaciakvStores(l)) {

			nodoKv *actual;

			actual = l -> fin;

			if (l -> inicio == actual) {

				l -> fin = l -> inicio = NULL;

			}
			else {

				l -> fin = actual -> previo;

				actual -> previo = NULL;

				l -> fin -> siguiente = NULL;

			}

			liberarMemoriakvStore(l, actual);

		}

	}

}

void eliminarkvStore(listakvStore *l, nodoKv *nodo) {

	if (l != NULL) {

		if (existenodoKv(l, nodo)) {

			if (nodo == l -> inicio) {

				eliminarPrimerkvStore(l);

			}
			else if (nodo == l -> fin) {

				eliminarUltimokvStore(l);

			}
			else {

				nodoKv *tempPrevio = nodo -> previo;

				nodoKv *tempSiguiente = nodo -> siguiente;

				tempPrevio -> siguiente = tempSiguiente;

				tempSiguiente -> previo = tempPrevio;

				nodo -> previo = NULL;

				nodo -> siguiente = NULL;

				liberarMemoriakvStore(l, nodo);

			}

		}

	}

}

void eliminarkvStoreXContenido(listakvStore *l, char *id) {

	if (l != NULL && id != NULL) {

		nodoKv *eliminar = obtenernodoKv(l, id);

		if (eliminar != NULL) {

			eliminarkvStore(l, eliminar);

		}

	}

}

kvStore *obtenerkvStoreXContenido(listakvStore *l, char *id) {

	if (l != NULL && id != NULL) {

		nodoKv *result = obtenernodoKv(l, id);

		if (result != NULL) {

			return result -> tabla;

		}

		return NULL;
	}

	return NULL;

}

bool existenodoKv(listakvStore *l, nodoKv *nodo) {

	if (l != NULL) {

		nodoKv *puntero;

		for(puntero = obtenerPrimerkvStore(l); puntero != NULL; puntero = puntero -> siguiente) {

			if (puntero == nodo) {

				return true;

			}

		}

		return false;

	}

	return false;

}

bool existekvStoreEnLista(listakvStore *l, kvStore *posiblekvStore) {

	if (l != NULL && posiblekvStore != NULL) {

		nodoKv *iterador;

		for (iterador = obtenerPrimerkvStore(l); iterador != NULL; iterador = iterador -> siguiente) {

			kvStore *kvStorePuntero = iterador -> tabla;

			return (strcmp(kvStorePuntero -> id, posiblekvStore -> id) == 0);

		}

		return false;

	}

	return false;

}

void liberarMemoriakvStore(listakvStore *l, nodoKv *n) {

	if (l != NULL) {

		// if (l -> liberar) {

		// 	l -> liberar(n -> tabla);

		// }

		//free(n -> tabla);
		free(n);



		l -> tamanioLista--;

	}

}


void imprimirlistakvStore(listakvStore *l) {

	printf("\n====kvStores====\n\n");

	if (obtenerCantidadkvStores(l) > 0) {
		
		nodoKv *puntero;


		for(puntero = obtenerPrimerkvStore(l); puntero != NULL; puntero = puntero -> siguiente) {

			kvStore *c;

			c = puntero -> tabla;

			printf("id: %s\n", c -> id);

			printf("\n");

		}

	}

	else {

		printf("\tNo existen kvStores para presentar...\n");

	}

}


void imprimirlistakvStoreDescripcion(listakvStore *l) {

	printf("\n====kvStores Agendadas====\n\n");

	if (obtenerCantidadkvStores(l) > 0) {
		
		nodoKv *puntero;

		int nkvStore;

		nkvStore = 1;

		for(puntero = obtenerPrimerkvStore(l); puntero != NULL; puntero = puntero -> siguiente) {

			kvStore *c;

			c = puntero -> tabla;

			printf("\t%d). %s\n", nkvStore, c -> id);

			nkvStore++;

		}

		printf("\t%d). Salir\n\n", nkvStore++);

	}

	else {

		printf("\tNo existen kvStores para presentar...\n");

	}

}


nodoKv * buscarnodoKv(listakvStore *l, int numerokvStore) {

	if (l != NULL && numerokvStore > 0) {

		int i = 1;

		nodoKv *ptr;

		for (ptr = obtenerPrimerkvStore(l); ptr != NULL; ptr = ptr -> siguiente) {

			if (i == numerokvStore) {

				return ptr;

			}

			i++;

		}

		return NULL;

	}

	return NULL;

}

nodoKv * obtenernodoKv(listakvStore *l, char *id) {

	if (l != NULL && id != NULL) {

		nodoKv *ptr;

		for (ptr = obtenerPrimerkvStore(l); ptr != NULL; ptr = ptr -> siguiente) {

			if ((strcmp(ptr -> tabla ->id, id) == 0)) {

				return ptr;

			}

		}

		return NULL;

	}

	return NULL;

}


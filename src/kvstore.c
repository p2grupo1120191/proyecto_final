#include <netinet/in.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/resource.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>             /* for convenience */
#include <stddef.h>             /* for offsetof */
#include <signal.h>             /* for SIG_ERR */ 
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "nodoKvstore.h"


#define BUFFERSIZE 1024



void *client_handler(void *ptr);

//Funcion de ayuda para setear la bandera close on exec
void set_cloexec(int fd){
	if(fcntl(fd, F_SETFD, fcntl(fd, F_GETFD) | FD_CLOEXEC) < 0){
		printf("error al establecer la bandera FD_CLOEXEC\n");	
	}
}


//Funcion para inicializar el servidor
int initserver(int type, const struct sockaddr *addr, socklen_t alen, int qlen){

	int fd;
	int err = 0;
	
	if((fd = socket(addr->sa_family, type, 0)) < 0)
		return -1;
		
	if(bind(fd, addr, alen) < 0)
		goto errout;
		
	if(type == SOCK_STREAM || type == SOCK_SEQPACKET){
		
			if(listen(fd, qlen) < 0)
				goto errout;
	}
	return fd;
	errout:
	err = errno;
	close(fd);
	errno = err;
	return (-1);
}
pthread_mutex_t block;
listakvStore *l;
int numero_buckets;

int main( int argc, char *argv[]){

	if(argc != 4){
		printf( "Uso: ./servidor <numero de host> <numero de puerto> <numero_buckets>\n");
		exit(1);
	}

	int puerto = atoi(argv[2]);
	numero_buckets = atoi(argv[3]);

	int sockfd;
	//Direccion del servidor
	struct sockaddr_in direccion_servidor;

	memset(&direccion_servidor, 0, sizeof(direccion_servidor));	//ponemos en 0 la estructura direccion_servidor

	//llenamos los campos
	direccion_servidor.sin_family = AF_INET;		//IPv4
	direccion_servidor.sin_port = htons(puerto);		//Convertimos el numero de puerto al endianness de la red
	direccion_servidor.sin_addr.s_addr = inet_addr(argv[1]) ;	//Nos vinculamos a la interface localhost o podemos usar INADDR_ANY para ligarnos A TODAS las interfaces

	

	//inicalizamos servidor (AF_INET + SOCK_STREAM = TCP)
	if( (sockfd = initserver(SOCK_STREAM, (struct sockaddr *)&direccion_servidor, sizeof(direccion_servidor), 1000)) < 0){	//Hasta 1000 solicitudes en cola 
		perror("Error al inicializar el servidor\n");
		exit(1);
	}		

	l = (listakvStore *)malloc(sizeof(listakvStore));

	crearlistakvStore(l, sizeof(kvStore), NULL);

	pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);

    pthread_mutex_init(&block,NULL);


	int *tmp;
	while(1)
    {	
    	pthread_t thread;
    	tmp = malloc(sizeof(int));
    	if(tmp==NULL){
    		fprintf(stderr, "No se pudo reservar memoria para nueva conexión\n");
    		sleep(1);
    	}else{
    		*tmp = accept(sockfd, NULL, 0);
        	pthread_create(&thread, &attr, client_handler, tmp);
    	}
        

	}
	
	return 0;
}

void *client_handler(void *ptr){

    
    int clfd = *(int *)ptr;
    free(ptr);
	char message[BUFFERSIZE]={0};
    char metodo[BUFFERSIZE];
    char kvs[BUFFERSIZE];
    char clave[BUFFERSIZE];
    char valor[BUFFERSIZE];

    printf("%s\n", "**********Cliente conectado*******");

    kvStore *kvstore;
    int nparams=0;
    int recv=0;
    while((recv=read(clfd, message, BUFFERSIZE)) > 0){

    	message[recv-1] = '\0';
		nparams = sscanf(message, "%[^,],%[^,],%[^,],%[^,]", metodo, kvs, clave, valor);


        if(strcmp(metodo, "PUT") == 0 && nparams ==4){
        	printf("Client ==>> %s, ", metodo);
			printf("kvs: %s, clave: %s, valor: %s\n", kvs,clave,valor);

			
			pthread_mutex_lock(&block);
			kvstore = obtenerkvStoreXContenido(l, kvs);
			pthread_mutex_unlock(&block);

			if(kvstore != NULL){
				put_kvStore(kvstore, clave, strdup(valor));
			}else{
				kvstore = crear_kvStore(kvs, numero_buckets);

				if(kvstore != NULL){
					put_kvStore(kvstore, clave, strdup(valor));
					insertarkvStoreAlFinal(l, kvstore);
				}
			}

		write(clfd, "OK\n", sizeof ("OK\n"));

        }
        else if(strcmp(metodo, "GET") == 0 && nparams ==3){

        	char *result;

        	printf("Client ==>> %s, ", metodo);
			printf("kvs: %s, clave: %s\n", kvs,clave);

			pthread_mutex_lock(&block);
			kvstore = obtenerkvStoreXContenido(l, kvs);
			pthread_mutex_unlock(&block);	

			if(kvstore != NULL){

				result = get_kvStore(kvstore, clave);

				if(result == NULL){
					char *error = "Error: No se encontro coincidencias\n";
					write(clfd, error, strlen (error));
				}
				else{
					char res[BUFFERSIZE] = {0};
					strcpy(res,result);
					strcat(res,"\n");
					write(clfd, res, strlen(res));
				}
			}else{
				char *error = "Error: no existe hashtable\n";
				write(clfd, error, strlen (error));
			}

			

        }
        else if(strcmp(metodo, "DELETE") == 0 && nparams ==2){

         	printf("Client ==>> %s, ", metodo);
			printf("kvs: %s\n", kvs);


			pthread_mutex_lock(&block);
			kvstore = obtenerkvStoreXContenido(l, kvs);
			eliminarkvStoreXContenido(l, kvs);
			pthread_mutex_unlock(&block);

			if(kvstore != NULL){

				free_kvStore(kvstore);
				write(clfd, "OK\n", strlen ("OK\n"));

			}else{
				char *error = "Error: no existe hashtable\n";
				write(clfd, error, strlen(error));
			}


        	
        }
        else if(strcmp(metodo, "REMOVE") == 0 && nparams ==3){
        	
        	printf("Client ==>> %s, ", metodo);
			printf("kvs: %s, clave: %s\n", kvs,clave);

        	pthread_mutex_lock(&block);
        	kvstore = obtenerkvStoreXContenido(l, kvs);
        	pthread_mutex_unlock(&block);
			
			if(kvstore != NULL){
				
				void *result = remove_kvStore(kvstore, clave);
				
				if(result == NULL){
					char *error= "Error: No se econtro coincidencias\n";
					write(clfd, error, strlen(error));
				}else{
					write(clfd, "OK\n", sizeof("OK\n"));
				}
			
			}else{
				char *error = "Error: no existe hashtable\n";
				write(clfd, error, strlen(error));
			}

		}else{
			char *error_msg= "Error: Parametros o metodo incorrectos\n";
			write(clfd,error_msg , strlen (error_msg));
		}

		memset(message,'\0',BUFFERSIZE);
	}

    printf("%s\n", "*******Cliente desconectado******");
    close(clfd);
    return NULL;
}



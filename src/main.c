#include <stdio.h>
#include "kv_store.h"

//funcion Main
int main(){
	kvStore *kvs=crear_kvStore("1",31);
	put_kvStore(kvs,"clave","valor",strlen("valor"));
	char *valor = get_kvStore(kvs,"clave",NULL);
	printf("%s\n",valor );
	free_kvStore(kvs);
	return 0;
}

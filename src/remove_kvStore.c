#include "kv_store.h"
//funcion remove_kvstore
//i se hace GET, REMOVE, DELETE a un kv_store que no existe, retornar error.
void *remove_kvStore(kvStore *kvs, char *clave){
	if(kvs==NULL || kvs->buckets == NULL || clave == NULL || clave[0]=='\0'){
		return NULL;
	}
	unsigned long result = hash_kvStore((unsigned char *)clave)%(unsigned long)kvs->numeroBuckets;

	//int kth_lock = result / 100;

	if(kvs->buckets[result]==NULL||kvs->buckets[result]->valor == NULL){
		return NULL;
	}

	pthread_mutex_lock(&kvs->mutex);
	void *val = kvs->buckets[result]->valor;
	free(kvs->buckets[result]->clave);
	kvs->buckets[result]->clave = NULL;
	kvs->buckets[result]->valor = NULL;

	pthread_mutex_unlock(&kvs->mutex);
	
	kvs->elementos--;
	return val;
}

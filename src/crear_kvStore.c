#include "kv_store.h"

kvStore* crear_kvStore(char *id, int n_buckets){
	if(id==NULL || id[0]=='\0' || n_buckets<1){
		return NULL;
	}

	kvStore *kvs = calloc(1,sizeof(kvStore));
	if(kvs==NULL){
		return NULL;
	}
	kvs->buckets = calloc(n_buckets,sizeof(kvObjeto *));
	if(kvs->buckets==NULL){
		free(kvs);
		return NULL;
	}
	kvs->id = strdup(id);
	if(kvs->id==NULL){
		free(kvs->buckets);
		free(kvs);
		return NULL;
	}
	for(int i = 0;i<n_buckets;i++){
		kvs->buckets[i] =  calloc(1,sizeof(kvObjeto));
	}
	kvs->numeroBuckets = n_buckets;

	// if(kvs->numeroBuckets > 100){
	// 	kvs->k = kvs->numeroBuckets / 100;
	// }else{
	// 	kvs->k = 1;
	// }

	// for(int i = 0; i < (kvs->k); i++){
	pthread_mutex_init(&kvs->mutex, NULL);
	// }
	pthread_mutex_init(&kvs->destroy_mutex, NULL);

	return kvs;
}

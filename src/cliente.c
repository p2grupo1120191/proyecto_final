#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#define BUFFERSIZE 1024

#define BUFLEN 4096 
#define MAXSLEEP 64

//funciones cliente
int connect_retry( int domain, int type, int protocol, 	const struct sockaddr *addr, socklen_t alen){
	
	int numsec, fd; /* * Try to connect with exponential backoff. */ 

	for (numsec = 1; numsec <= MAXSLEEP; numsec <<= 1) { 

		if (( fd = socket( domain, type, protocol)) < 0) 
			return(-1); 

		if (connect( fd, addr, alen) == 0) { /* * Conexión aceptada. */ 
			return(fd); 
		} 
		close(fd); 				//Si falla conexion cerramos y creamos nuevo socket

		/* * Delay before trying again. */
		if (numsec <= MAXSLEEP/2)

			sleep( numsec); 
	} 
	return(-1); 
}

int main( int argc, char *argv[]) { 



	int sockfd;

	if(argc != 3){
		printf("Uso: ./cliente <ip> <puerto>\n");
		exit(-1);
	}


	int puerto = atoi(argv[2]);


	//Direccion del servidor
	struct sockaddr_in direccion_cliente;

	memset(&direccion_cliente, 0, sizeof(direccion_cliente));	//ponemos en 0 la estructura direccion_servidor

	//llenamos los campos
	direccion_cliente.sin_family = AF_INET;		//IPv4
	direccion_cliente.sin_port = htons(puerto);		//Convertimos el numero de puerto al endianness de la red
	direccion_cliente.sin_addr.s_addr = inet_addr(argv[1]) ;	//Nos tratamos de conectar a esta direccion

	//AF_INET + SOCK_STREAM = TCP

	if (( sockfd = connect_retry( direccion_cliente.sin_family, SOCK_STREAM, 0, (struct sockaddr *)&direccion_cliente, sizeof(direccion_cliente))) < 0) { 
		printf("falló conexión\n"); 
		exit(-1);
	} 


    
    char buffer[BUFFERSIZE];
  	while (fgets(buffer, BUFFERSIZE, stdin) != NULL) {

  		//buffer[strcspn(buffer, "\n")] = '\0';
  		write(sockfd, buffer, strlen(buffer));
  		memset(buffer,0,BUFFERSIZE);
		read(sockfd, buffer, BUFFERSIZE);
  		printf("Message: %s", buffer);
  		//buffer2[strcspn(buffer2, "\n")] = '\0';

  	}

	close(sockfd);
}



#include "kv_store.h"

unsigned long hash_kvStore(unsigned char *str){
	unsigned long hash = 5381;
	int c;
	while ((c = *str++))
		hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
	return hash;
}

#ifndef KVSTORE_H
#define KVSTORE_H

#include <stdio.h>

#include <stdlib.h>
#include <string.h>
#include <pthread.h>


typedef struct kvTDA{
	char *clave;
	char *valor;
}kvObjeto;


typedef struct kvStoreTDA{
	char *id;
	int elementos;
	int numeroBuckets;
	kvObjeto **buckets;
	pthread_mutex_t mutex;
	pthread_mutex_t destroy_mutex;
}kvStore;

kvStore* crear_kvStore(char *id, int n_buckets);
void free_kvStore(kvStore *kvs);
void *get_kvStore(kvStore *kvs, char *clave);
unsigned long hash_kvStore(unsigned char *str);
void put_kvStore(kvStore *kvs, char *clave, void *valor);
void *remove_kvStore(kvStore *kvs, char *clave);



#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "kv_store.h"
// #include <nodoContacto.h>
// #include <fecha.h>

//Implementacion estructuras nodokv
typedef void (*liberarEspaciokvStore)(kvStore *);

typedef struct nodoKv {

	struct nodoKv *previo;
	kvStore *tabla;
	struct nodoKv *siguiente;

} nodoKv; 



typedef struct listakvStore {
	
	nodoKv *inicio;
	nodoKv *fin;
	int tamanioLista;
	int tamanioElemento;
	liberarEspaciokvStore liberar;

} listakvStore;

//Declaracion de Funciones
void crearlistakvStore(listakvStore *l, int tamanioElemento, liberarEspaciokvStore liberar);
void destruirlistakvStore(listakvStore *l);
nodoKv * crearnodoKv(listakvStore *l, kvStore *c);

nodoKv *obtenerPrimerkvStore(listakvStore *l);
nodoKv *obtenerUltimokvStore(listakvStore *l);
kvStore *obtenerkvStoreXContenido(listakvStore *l, char *id);


int obtenerCantidadkvStores(listakvStore *l);
bool estaVaciakvStores(listakvStore *l);

void insertarkvStoreAlInicio(listakvStore *l, kvStore *c);
void insertarkvStoreAlFinal(listakvStore *l, kvStore *c);
void insertarkvStoreDespuesDe(listakvStore *l, kvStore *c, kvStore *nC);

void eliminarPrimerkvStore(listakvStore *l);
void eliminarUltimokvStore(listakvStore *l);
void eliminarkvStore(listakvStore *l, nodoKv *nodo);
void eliminarkvStoreXContenido(listakvStore *l, char *id);

bool existenodoKv(listakvStore *l, nodoKv *nodo);
bool existekvStoreEnLista(listakvStore *l, kvStore *posiblekvStore);

void liberarMemoriakvStore(listakvStore *l, nodoKv *n);
void imprimirlistakvStore(listakvStore *l);
void imprimirlistakvStoreDescripcion(listakvStore *l);

nodoKv * buscarnodoKv(listakvStore *l, int numerokvStore);
nodoKv * obtenernodoKv(listakvStore *l, char *id);

